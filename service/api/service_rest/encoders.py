from common.json import ModelEncoder
from .models import Technician, ServiceAppointment
from django.core.serializers.json import DjangoJSONEncoder
from json import JSONEncoder
from datetime import date,time



class DateTimeEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o,date) or isinstance(o,time):
            return str(o)



class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["name","employee_number"]


class AppointmentListEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = ["customer_name","reason","vin","status","id"]
    
    def get_extra_data(self, o):
        return {"technician":o.technician.name,"date":str(o.date),"time":str(o.time)}