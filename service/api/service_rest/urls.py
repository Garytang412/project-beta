from django.urls import path
from .views import api_list_technicians, api_list_appointments,api_update_appointments,api_finish_appointments, api_cancel_appointments,api_list_autoVOs


# localhost:8080/api 
urlpatterns = [
    # list/post technicians 
    path("technicians/", api_list_technicians),
    # detail technicians  may not need it for now
    # path("technicians/<int:pk>"),

    # list/post appointments
    path("appointments/",api_list_appointments),
    
    # update a appointment
    path("appointments/<int:pk>/",api_update_appointments),

    # finish the appointment:
    path("appointments/<int:pk>/finish/",api_finish_appointments),
    # cancel the appointment:
    path("appointments/<int:pk>/cancel/",api_cancel_appointments),

    # list api all the autovos:
    path("autovos/",api_list_autoVOs),

]