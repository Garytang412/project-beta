import React from 'react';
import { useState, useEffect } from 'react';

export default function Sales() {
    const [sales, setModels] = useState([]);
    useEffect(() => {
        fetch("http://localhost:8090/api/sale_records/")
        .then(res => res.json())
        .then(data => {setModels(data.sales);
        console.log(data)})
    }, [])

    return(
        <div>
            <table className='table'>
                <thead>
                    <tr>
                        <th scope='col'>Seller Name</th>
                        <th scope='col'>Employee Number</th>
                        <th scope='col'>Purchaser Name</th>
                        <th scope='col'>Automobile VIN</th>
                        <th scope='col'>Price</th>
                    </tr>
                </thead>
                <tbody>
                {sales.map(e => {return (
                <tr key={e.automobile.vin}>
                    <th>{e.sales_person.name}</th>
                    <th>{e.sales_person.employee_no}</th>
                    <th>{e.customer.name}</th>
                    <th>{e.automobile.vin}</th>
                    <th>{`$${e.sale_price}.00`}</th>
                </tr>
                )})}
                </tbody>
            </table>
        </div>
    )
}