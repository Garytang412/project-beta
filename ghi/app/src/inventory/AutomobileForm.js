import React from 'react'
import { useState, useEffect } from 'react';
export default function AutomobileForm() {
    const [models, setModels] = useState([]);
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [color, setColor] = useState('');
    const [modelId, setModelId] = useState('');
    const [hide, setHide] = useState("d-none");


    useEffect(() => {
        const modelsUrl = "http://localhost:8100/api/models/";
        try {
            fetch(modelsUrl)
                .then(res => res.json())
                .then(data => setModels(data.models))
        }
        catch (e) {
            console.log("models fetch error:", e)
        }
    }, [])

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(year, vin, color, modelId);
        const postUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                color: color,
                year: year,
                vin: vin,
                model_id: modelId
            }),
            headers: { "Content-Type": "application/json" }
        };
        fetch(postUrl, fetchConfig)
            .then(res => res.json())
            .then(data => console.log('added!'))
            .catch(e => console.log("post error", e));
        setYear("");
        setColor("");
        setVin("");
        setModelId("");
        setHide('');


    }



    return (
        <div className="container col-6 mt-3 shadow p-3">
            <h1>Add an automobile to inventory</h1>
            <form onSubmit={handleSubmit}>
                <div className="form-group pb-3">
                    <label htmlFor="color">Color:</label>
                    <input onChange={e => setColor(e.target.value)} type="text" className="form-control" value={color} placeholder="Color" name="color" id='color' />
                </div>

                <div className="form-group pb-3">
                    <label htmlFor="year">year:</label>
                    <input onChange={e => setYear(e.target.value)} type="text" className="form-control" value={year} placeholder="year" name="year" id='year' />
                </div>

                <div className="form-group pb-3">
                    <label htmlFor="VIN">VIN:</label>
                    <input onChange={e => setVin(e.target.value)} type="text" className="form-control" value={vin} placeholder="VIN" name="vin" id='VIN' />
                </div>

                <div className="form-group pb-3">
                    <label value={modelId} htmlFor="model">Select a Model:</label>
                    <select onChange={e => setModelId(e.target.value)} className="form-control" value={modelId} id="model" name="model">
                        <option value={''}>Chose a model</option>
                        {models.map(m => <option key={m.name} value={m.id}>{m.name}</option>)}
                    </select>
                </div>
                <button type="submit" className="btn btn-primary">Create</button>
            </form>
            <div className={`fs-4 ${hide}`}>Added!</div>
        </div>
    )
}