import React from 'react'
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';




export default function Technicians() {
    const [techs, setTechs] = useState([]);
    const [searchParam, setSearchParam] = useState('');
    useEffect(() => {
        const techListUrl = "http://localhost:8080/api/technicians/";
        fetch(techListUrl)
            .then(resp => resp.json())
            .then(data => setTechs(data.technicians))
    }, [])


    return (
        <div className='container'>
            <div className='fs-2 fw-bold '>Service Technichans:</div>
            <div className='mb-3'>
                <div className="fs-4">serch tech</div>
                <input onChange={e => setSearchParam(e.target.value)} type="text" />
            </div>
            <Link className='btn btn-primary' to="/service/technicians/new">Add Technician</Link>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Employee Number:</th>
                    </tr>
                </thead>
                <tbody>
                    {techs.filter(tech => { if (tech.name.toLowerCase().includes(searchParam.toLowerCase())) return tech }).map(tech => <tr key={tech.employee_number}><td>{tech.name}</td><td>{tech.employee_number}</td></tr>)}
                </tbody>
            </table>
        </div>
    )
}
