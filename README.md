# CarCar

Team:

* Gary - Service Microservice
* Jason - Sales microservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.
## Design:

![Service_Design](Carcar_service.png)

- appointment shoud have 3 status: ('0',"submited"),('1',"finished"),('2',"cancelled"):
    - two ways to change the status:
        1. front end HandleClick put to update appointment status with full all fields of information
        2. create cancel/finish methods for Appointment model, and api only change status to cancelld or finished. front end put a url to change the status.  --> for now

- two ways to filter technicians/vin service records:
    1. front end get all data filter before map. --> for now
    2. create an api to return fitered data only.

- two ways to check if need to trate as VIP:
    1. front end check if it is vip before display. --> for now.
    2. check if it is vip on post api based on the vin, before create.

- Models:
    1. AutomobileVO: 
        - polling automobile data from inventory api.
        - use to check if a car was bought from us (trated as VIP), when make a service appointment()
    2. Technician:
        - add/ list technician
        - list all the technicians as options to selete when make a new service appointment
    3. ServiceAppointment:
        - hold all the service appointments record
        - has 3 status: ('0',"submited"),('1',"finished"),('2',"cancelled")
            - have finish/cancel methods and api to change the status to finished or cancelled
        - need to hold a vin to check if the car should be trated as VIP.(by checking if vin in automobileVO list)
- APIs:
    1. list/post technicians (don't need to update/delete for now)
    2. list all the AutoVOs with their VIN (to check if need to be trated as VIP when make an appointment)
    3. list/post appointments
        - finish an appointment
        - cancel an appointment
- React:
    - Technicians:
        - table show Technicians. / filter search tech by name
        - Technician Form: add new technician
    - Appointments: fetch all the appontments record data and all autoVO, check if is_vip and pass map pass to Appointment component using props. Search vin record
        - Appointment: handle display appointment details. using swich case to display different status. handle cancel/finish by fetch api on btn Click. 
        - Appointment Form: fetch technicians list and map for technician select options. set and change show and hide states for hide/display form and success page.


## Sales microservice

Explain your models and integration with the inventory
microservice, here.

Models:

AutomobileVO:
    - the value object for the automobiles found in inventory
    - this was used for:
        - listing the sales records
        - filtering if the car was sold or not in sales records and creating a sale
        - It was linked throughout the app through it's unique HREF

SalesPerson:
    - the model for a sales person working at the CarCar dealership
    - it keeps track of the employee's name, and an employee number
    - this model was used in the following:
        - the create sales form, to decide who sold the car
        - the create sales form as a foreign key to grab the unique id to create a sale
        - the list sales portion of the app to keep track of the employee's sales
    
PotentialCustomer:
    - the model for a potential customer shopping at the CarCar dealership
    - keeping track of name, address, and phone_no
    - the model is used in the following:
        - the create sales form as a foreign key to grab the unique id to create a sale
        - the list sales form to keep track of which employee sold to who

Sale:
    - the model for a sale, which displays sales price and takes in:
        - AutomobileVO
        - PotentialCustomer
        -SalesPerson
    as Foreign Keys. 
    -each of these foreign keys have an identifier which is needed to create a sale.
